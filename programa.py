import matplotlib.pyplot as plt
import pandas as pd

plt.rcdefaults()

dataset = pd.read_csv('./dataset.csv', sep=',')

date_cases = dataset[['date', 'cases']].groupby('date')['cases'].sum()

date_cases.plot.line()

plt.xlabel('Data')
plt.ylabel('Quantidade')
plt.title('Número de casos de COVID-19 por data no Brasil')

plt.show()

plt.close()

region_cases = dataset[['region', 'cases']].groupby('region')['cases'].sum()
region_cases.plot.pie(y='cases', figsize=(5,5), autopct='%1.1f%%', legend=True, startangle=90, shadow=False)

plt.xlabel('Distribuição')
plt.ylabel('')
plt.title('Distribuição de casos por região')

plt.show()

plt.close()

data_case_deaths = dataset[['date', 'cases', 'deaths']].groupby('date').agg({
    'cases' : 'sum',
    'deaths' : 'sum'
})

data_case_deaths[::7].plot.barh()

plt.xlabel('Quantidade')
plt.ylabel('Data')
plt.title('Relação entre casos e mortes pode data nmo Brasil')
plt.legend(['casos', 'mortes'])

plt.show()